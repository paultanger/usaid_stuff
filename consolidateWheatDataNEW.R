sourcepath = "~/Desktop/Rgit/usaid_stuff/"
setwd(sourcepath)
source("RUNFIRST.R")
options(scipen = 999)

# read data
datapath = "/Users/paultanger/odrive/Google Drive/USAID/crop data analysis/for front office"
setwd(datapath)

##################################################
# load FAO data including prod and area as well as yield - to be able to compare FTFMS numbers
FAOProdAreaYield = read.csv("FAOSTAT_data_6-30-2017_CropsProdYieldArea.csv", header=T)

# fix cols
# as.data.frame(colnames(FAOProdAreaYield))

FAOProdAreaYield = FAOProdAreaYield[,c(4,6,8,10,12)]

colnames(FAOProdAreaYield) = c("Country", "Parameter", "Crop", "Year", "Value")

# make wide to convert hectograms/hectare to MT/ha
library(reshape2)
FAOProdAreaYieldWide = dcast(FAOProdAreaYield, Country + Crop + Year ~ Parameter, sum)
# fix cols
# as.data.frame(colnames(FAOProdAreaYieldWide))
colnames(FAOProdAreaYieldWide) = c("Country", "Crop", "Year", "FAOArea (ha)", "FAOProduction (MT)", "Yield (hg/ha)")

FAOProdAreaYieldWide$FAOYield = FAOProdAreaYieldWide$`Yield (hg/ha)` / 10000
FAOProdAreaYieldWide = FAOProdAreaYieldWide[,-6]
colnames(FAOProdAreaYieldWide) = c("Country", "Crop", "Year", "FAOArea (ha)", "FAOProduction (MT)", "FAOYield (MT/ha)")
FAOProdAreaYieldWide$Country = as.factor(FAOProdAreaYieldWide$Country)
FAOProdAreaYieldWide$Year = as.factor(FAOProdAreaYieldWide$Year)
##################################################

# load USDA FAS data
# FAS = read.csv("FASCropYieldCountriesThruM.csv", header=T, na.strings="", stringsAsFactors = F)
# # for some reason the website  didn't get all countries in one export
# FAS2 = read.csv("FASCropYieldCountriesNThruZ.csv", header=T, na.strings="", stringsAsFactors = F)
# FAS = rbind(FAS, FAS2)
# 
# # add prod and yield data..
# FAS3 = read.csv("FASprodarea.csv", header=T, na.strings="", stringsAsFactors = F)
# 
# FAS = rbind(FAS, FAS3)

# add rough rice prod
# note that because other data sets from FAS include coffee, and FAS warns:
# Please note:: When selecting "Coffee,Green" and/or "Sugar,Centrifugal" with Other commodities, it is possible for the marketing years to become misaligned.
# data that is marketing year 2011/2012 (what I've been calling 2012) is really 2013
# there isn't an easy fix for this, so just download rice data..
# also, comparing to FAO, it is best to use the first year so 2009/2010 is 2009
# so download data for 2010-2016



# FAS = read.csv("FASrice.csv", header=T, na.strings="", stringsAsFactors = F)

FAS = read.csv("FASwheat2010-2016.csv", header=T, na.strings="", stringsAsFactors = F)


# FAS = rbind(FAS, FAS4)

FAS = FAS[,c(3,1,2,4:10)]

# everything is 1000 x ha or 1000 x MT except for coffee which is 1000 x 60 KG bags
# as.data.frame(colnames(FAS))
colnames(FAS) = c("Country", "Crop", "Parameter", seq(2010,2016))

# fix the fact that crop isn't populated in each row
# install.packages("zoo")
library(zoo)
FAS$Crop = as.character(FAS$Crop)
FAS$Crop[FAS$Crop == ""] <- "NA"
FAS$Crop = na.locf(FAS$Crop)

FAS$Parameter = as.character(FAS$Parameter)
FAS$Parameter[FAS$Parameter == ""] <- "NA"
FAS$Parameter = na.locf(FAS$Parameter)

# melt it
FASlong = melt(FAS, id.vars = c("Country", "Crop", "Parameter"))

# check it
filename = addStampToFilename("FASlong", "tsv")
# write.table(FASlong, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# fix units (and get rid of commas)
FASlong$value = as.numeric(gsub(",", "", FASlong$value))
# FASlong$Parameter = as.factor(FASlong$Parameter)

# for prod
FASlong[(FASlong$Parameter == "Production") & (FASlong$Crop != "Coffee, Green"),]$value = FASlong[(FASlong$Parameter == "Production") & (FASlong$Crop != "Coffee, Green"),]$value * 1000

# for area
FASlong[FASlong$Parameter == "Area Harvested",]$value = FASlong[FASlong$Parameter == "Area Harvested",]$value * 1000

filename = addStampToFilename("FASlong", "tsv")
# write.table(FASlong, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# fix col names
# as.data.frame(colnames(FASlong))
colnames(FASlong) = c("Country", "Crop", "Parameter", "Year", "Value")

# make it wide
FASwide = dcast(FASlong, Country + Crop + Year ~ Parameter, sum, value.var = "Value")

colnames(FASwide) = c("Country", "Crop", "Year", "FASArea(ha)", "FASProd(MT)", "FASyield (MT/ha)")

# FASlong$Crop = as.factor(FASlong$Crop)
# FASlong$Country = as.factor(FASlong$Country)

# FASmaizelong$Year = substring(as.character(FASmaizelong$Year), 2)
##################################################

# add FAO data to this

# check and standardize country and crop names
# FAOcountries = levels(FAOProdAreaYieldWide$Country)
# FAScountries = levels(FASlong$Country)
# 
# setdiff(FAOcountries, FAScountries)
# 
# filename = addStampToFilename("FAOcountries", "tsv")
# # write.table(FAOcountries, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)
# filename = addStampToFilename("FAScountries", "tsv")
# # write.table(FAScountries, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

FAOCrops = levels(FAOProdAreaYieldWide$Crop)
FASCrops = levels(FASlong$Crop)

filename = addStampToFilename("FAOcrops", "tsv")
# write.table(FAOCrops, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)
filename = addStampToFilename("FAScrops", "tsv")
# write.table(FASCrops, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# subset rice data
FASlong = subset(FASlong, Crop == "Wheat")


# make it wide
FASwide = dcast(FASlong, Country + Crop + Year ~ Parameter, sum, value.var = "Value")

colnames(FASwide) = c("Country", "Crop", "Year", "FASArea(ha)", "FASProd(MT)", "FASyield (MT/ha)")

# Merge
FASandFAO = merge(FASwide, FAOProdAreaYieldWide, all=T)
FASandFAO$Country = as.factor(FASandFAO$Country)
# levels(droplevels(FASandFAO$Country))

# export crop types
# FASandFAO$Crop = as.factor(FASandFAO$Crop)
FASandFAOCrops = levels(FASandFAO$Crop)
filename = addStampToFilename("FASandFAOCrops", "tsv")
# write.table(FASandFAOCrops, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# export
filename = addStampToFilename("FASandFAOdata", "tsv")
# write.table(FASandFAO, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)
##################################################

# fix country name - do this later.. none of these are FTFMS countries anyway..

# levels(FASandFAO$Country)[levels(FASandFAO$Country)=="Kyrgyzstan"] <- "Kyrgyz Republic"
# levels(FASandFAO$Country)[levels(FASandFAO$Country)=="Bolivia (Plurinational State of)"] <- "Bolivia"
# levels(FASandFAO$Country)[levels(FASandFAO$Country)=="Burkina"] <- "Burkina Faso"
# levels(FASandFAO$Country)[levels(FASandFAO$Country)=="China, mainland"] <- "China"
# korea
# gambia
# ivory coast
# iran
# laos
# us
# vietnam
# venesuela

##################################################

# pull data from FTFMS
#that data was a mess.. cleaned up in excel..
FTFdata = read.csv("World_data_2017_05_30_FTFMS_v9.csv", header=T)

# reorg columns
as.data.frame(colnames(FTFdata))
FTFdata = FTFdata[,c(8:12,1:7)]

# melt year
FTFdatalong = melt(FTFdata, id.vars = c(1:6))

# rename cols
# as.data.frame(colnames(FTFdatalong))
# colnames(FTFdatalong)[7] = "Year"

# fix year
FTFdatalong$Year = substring(as.character(FTFdatalong$variable), 2)
FTFdatalong = FTFdatalong[,c(1:6,9,8)]

# keep data where it is not null
FTFdatacleaned = subset(FTFdatalong, !is.na(FTFdatalong[,8]))

# delete data with zeros
FTFdatacleaned2 = subset(FTFdatacleaned, FTFdatacleaned[,8] != 0)

# export to check this in excel
filename = addStampToFilename("FTFdatacleaned2", "tsv")
# write.table(FTFdatacleaned2, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

##################################################

# combine similar crop types here
FTFdatacleaned2$Crop = gsub("\\[.*$", "", FTFdatacleaned2$Crop)
FTFdatacleaned2$Crop = trimws(FTFdatacleaned2$Crop)

# update from list that matches to FAS and FAO data
RevisedCropLabels = read.csv("FTFMSCrops_To_FASFAO.csv")
FTFdatacleaned3 = merge(FTFdatacleaned2, RevisedCropLabels, all.x=T)
# as.data.frame(colnames(FTFdatacleaned3))
# remove old crop col
FTFdatacleaned4 = FTFdatacleaned3[,c(2:4,9,5:8)]
colnames(FTFdatacleaned4)[4] = "Crop"
# as.data.frame(colnames(FTFdatacleaned4))

##################################################
# ok, lets sum the types together for each parameter..
library(plyr)

collapsestats <- aggregate(value ~ Region + Country + IM + Crop + Parameter + Year, data=FTFdatacleaned4, FUN=function(x) c(length(x), sum(x), sd(x), sd(x)/sqrt(length(x))) )
collapsestats <- cbind(collapsestats[,1:6], as.data.frame(collapsestats[,7]))
names(collapsestats) <- c("Region", "Country", "IM", "Crop", "Parameter", "Year", "n", "total", "SD", "SE")

##################################################

# make it wide and remove cols
# as.data.frame(colnames(collapsestats))
# also, could have done the sum here of each disaggregated instead of above but I did it above to check what had multiple values being added together
collapsestats = collapsestats[,c(1:6,8)]
FTFdataWide = dcast(collapsestats, Region + Country + IM + Crop + Year ~ Parameter, sum, value.var = "total")

##################################################
# this is where we need to fix the headings etc
# as.data.frame(colnames(FTFdataWide))
colnames(FTFdataWide) = c("Region",
                          "Country",
                          "IM",
                          "Crop",
                          "Year",
                          "Beneficiaries",
                          "input_Cost_USD",
                          "sales_MT",
                          "sales_unknown",
                          "sales_other",
                          "prod_MT",
                          "prod_unknown",
                          "prod_other",
                          "area_ha",
                          "sales_USD")

# seems like these were just collected in diff years.. we can combine, check in excel, and error check later assumptions about mt, etc
FTFdataWide$sales_calc_MT = FTFdataWide$sales_MT + FTFdataWide$sales_unknown + FTFdataWide$sales_other
FTFdataWide$prod_calc_MT = FTFdataWide$prod_MT + FTFdataWide$prod_unknown + FTFdataWide$prod_other
FTFdataWide$yield_calc = FTFdataWide$prod_calc_MT / FTFdataWide$area_ha

filename = addStampToFilename("FTFdata", "tsv")
# write.table(FTFdataWide, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# keep first part of crop column
levels(FTFdataWide$Crop)
# FTFdataWide$Crop = gsub("\\[.*$", "", FTFdataWide$Crop)
# FTFdataWide$Crop = trimws(FTFdataWide$Crop)

# prep crop to match other data
FTFdataWide$Crop = as.factor(FTFdataWide$Crop)
FTFMSCrops = levels(FTFdataWide$Crop)
filename = addStampToFilename("FTFMSCrops", "tsv")
# write.table(FTFMSCrops, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# collapse at country level
# maizestats <- aggregate(value ~ Region + Country + Crop + variable + Year, data=maizelong, FUN=function(x) c(length(x), sum(x), sd(x), sd(x)/sqrt(length(x))) )
# maizestats <- cbind(maizestats[,1:5], as.data.frame(maizestats[,6]))
# names(maizestats) <- c("Region", "Country", "Crop", "Parameter", "Year", "n", "total", "SD", "SE")

##################################################
# add FAS and FAO data
# alldata = merge(FTFdataWide, FASandFAO, all=T)

# this is where to check country, crop, etc.. we will want all.x=T eventually but lets make sure we aren't losing data first..
# filename = addStampToFilename("alldatatocheck", "tsv")
# write.table(alldata, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

alldata = merge(FTFdataWide, FASandFAO, all.x=T)

# round yield calcs
alldata$yield_calc = round(alldata$yield_calc, 2)
filename = addStampToFilename("alldata", "tsv")
# write.table(alldata, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# sort cols
# as.data.frame(colnames(alldata))
# alldata= alldata[,c(4,1,5,2,3,6,7,8:10,16,15,11:13,17,21,14,20,18,19,22)]

# order data
# alldata = alldata[with(alldata, order(Region, Country, Crop, Year)),]

# filename = addStampToFilename("alldata", "tsv")
# write.table(alldata, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE, na="")


# subset Maize data
maize = subset(alldata, alldata$Crop == "Wheat")

# remove messy data
maize = subset(maize, maize$yield_calc < 20)
maize = subset(maize, maize$yield_calc > 0)

# try to summarize from here..
filename = addStampToFilename("maizedata_withFASprodarea", "tsv")
write.table(maize, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE, na="")

as.data.frame(colnames(maize))
maize = maize[,-c(18:24)]

# make long
maizelong = melt(maize[,-5], id.vars=c("Region", "Country", "Year", "Crop"))
# FAS yield was character..
maizelong$value = as.numeric(maizelong$value)

# collapse IM data together for each country
maizestats <- aggregate(value ~ Region + Country + Crop + variable + Year, data=maizelong, FUN=function(x) c(length(x), sum(x), sd(x), sd(x)/sqrt(length(x))) )
maizestats <- cbind(maizestats[,1:5], as.data.frame(maizestats[,6]))
names(maizestats) <- c("Region", "Country", "Crop", "Parameter", "Year", "n", "total", "SD", "SE")

# country_maize = ddply(maize, Crop ~ Parameter, summarize, 
#                      N = sum(!is.na(Value)),
#                      mean=mean(Value, na.rm=T),
#                      sd   = sd(Value, na.rm=T))

# make it wide
maizestats2 = maizestats[,c(1:5,7)]
maizestatswide = dcast(maizestats2, Region + Country + Crop + Year ~ Parameter, sum, value.var = "total")

# remove old FAS and FAO (since sums aren't correct)
as.data.frame(colnames(maizestatswide))
maizestatswide = maizestatswide[,c(1:16)]

# add FAS and FAO data...
maizedata = merge(maizestatswide, FASandFAO, all.x=T)

as.data.frame(colnames(maizedata))
maizedata = maizedata[,c(4,1,2,3,5:9,14,15,10:12,16,18,21,13,17,20,19,22)]

maizedata$FASyieldcheck = maizedata$`FASProd(MT)`/maizedata$`FASArea(ha)`
maizedata$FAOyieldcheck = maizedata$`FAOProduction (MT)`/maizedata$`FAOArea (ha)`
maizedata$FTFMSyield = maizedata$prod_calc_MT/maizedata$area_ha

maizedata = maizedata[with(maizedata, order(Region, Country, Year)),]

filename = addStampToFilename("maizedata_countrylevelnew", "tsv")
write.table(maizedata, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE, na="")
